import React, { Component } from 'react'

export default class WelcomePageComponent extends Component {

    nextPath(path) {
        this.props.history.push(path);
      }

    render() {
        return (
            <div className="text-center">
                <h1>Welcome</h1>
                <button className="btn btn-secondary btn-lg btn-block"
                onClick={() => this.nextPath('/students') }
                >Go to the list of students</button>
            </div>
        )
    }
}
