import React, { Component } from "react";
import StudentService from "../services/StudentService";

export default class ListStudentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      students: [],
    };
  }

  componentDidMount() {
    StudentService.getStudents().then((res) => {
      this.setState({ students: res.data });
    });
  }

  nextPath(path) {
    this.props.history.push(path);
  }

  onDelete(id) {
    StudentService.deleteStudent(id).then((res) => {
      this.setState({
        students: this.state.students.filter((student) => student.id != id),
      });
    });
  }

  onDetails(id) {
    this.nextPath(`studentDetails/${id}`);
  }

  render() {
    return (
      <div>
        <h2 className="text-center">Students:</h2>
        <div className="row">
          <button
            className="btn btn-secondary"
            onClick={() => this.nextPath("/student/-1")}
          >
            Add Student
          </button>
          <button
            className="btn btn-secondary"
            style={{ marginLeft: "10px" }}
            onClick={() => this.nextPath("/")}
          >
            Homepage
          </button>
        </div>
        <div className="row">
          <table className="table table-hover table-striped table-bordered table-dark">
            <thead>
              <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Email</th>
                <th>Age</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.students.map((student) => (
                <tr key={student.id}>
                  <td>{student.firstName}</td>
                  <td>{student.lastName}</td>
                  <td>{student.email}</td>
                  <td>{student.age}</td>
                  <td>
                    <button
                      className="btn btn-primary"
                      onClick={() => this.nextPath(`/student/${student.id}`)}
                    >
                      Update
                    </button>
                    <button
                      onClick={() => this.onDelete(student.id)}
                      className="btn btn-danger"
                      style={{ marginLeft: "10px" }}
                    >
                      Delete
                    </button>
                    <button
                      onClick={() => this.onDetails(student.id)}
                      className="btn btn-secondary"
                      style={{ marginLeft: "10px" }}
                    >
                      Details
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
