import React, { Component } from "react";
import StudentService from "../services/StudentService";

class CreateStudentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      firstName: "",
      lastName: "",
      email: "",
      age: "",
    };
  }

  componentDidMount() {
    if (this.state.id < 0) {
      return;
    } else {
      StudentService.getStudentById(this.state.id).then((res) => {
        let student = res.data;
        this.setState({
          firstName: student.firstName,
          lastName: student.lastName,
          email: student.email,
          age: student.age,
        });
      });
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    let student = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      age: this.state.age,
    };

    if (this.state.id < 0) {
      StudentService.createStudent(student).then((res) => {
        this.nextPath("/students");
      });
    } else {
      StudentService.updateStudent(student, this.state.id).then((res) => {
        this.nextPath("/students");
      });
    }
  };
  nextPath(path) {
    this.props.history.push(path);
  }

  getTitle() {
    if (this.state.id == -1) {
      return <h3>Create Student</h3>;
    } else {
      return <h3>Update Student</h3>;
    }
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="card col-md-6 offset-md-3 offset-md-3">
              {this.getTitle()}
              <div className="card-body">
                <form>
                  <div className="form-group">
                    <label>First name:</label>
                    <input
                      type="text"
                      placeholder="First Name"
                      name="firstName"
                      className="form-control"
                      value={this.state.firstName}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <label>Last name:</label>
                    <input
                      type="text"
                      placeholder="Last Name"
                      name="lastName"
                      className="form-control"
                      value={this.state.lastName}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <label>Email:</label>
                    <input
                      type="text"
                      placeholder="Email"
                      name="email"
                      className="form-control"
                      value={this.state.email}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <label>Age:</label>
                    <input
                      type="text"
                      placeholder="Age"
                      name="age"
                      className="form-control"
                      value={this.state.age}
                      onChange={this.handleChange}
                    />
                  </div>
                  <button
                    className="btn btn-success"
                    onClick={this.handleSubmit}
                  >
                    Save
                  </button>
                  <button
                    className="btn btn-danger"
                    style={{ marginLeft: "10px" }}
                    onClick={() => this.nextPath("/students")}
                  >
                    Cancel
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateStudentComponent;
