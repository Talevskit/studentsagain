import React, { Component } from "react";
import StudentService from "../services/StudentService";

class StudentDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      student: {},
    };
  }
  nextPath(path) {
    this.props.history.push(path);
  }

  componentDidMount() {
    StudentService.getStudentById(this.state.id).then((res) => {
      this.setState({ student: res.data });
    });
  }
  render() {
    return (
      <div className="card col-md-6 offset-md-3">
        <h3 className="text-center">Student details</h3>
        <div className="card-body">
          <div className="row">
            <label>Student first name:</label>
            <div>
              <strong>{this.state.student.firstName}</strong>
            </div>
          </div>
          <div className="row">
            <label>Student last name:</label>
            <div>
              <strong>{this.state.student.lastName}</strong>
            </div>
          </div>
          <div className="row">
            <label>Student email:</label>
            <div>
              <strong>{this.state.student.email}</strong>
            </div>
          </div>
          <div className="row">
            <label>Student age:</label>
            <div>
              <strong>{this.state.student.age}</strong>
            </div>
          </div>
        </div>

        <button
          className="btn btn-secondary"
          onClick={() => this.nextPath("/students")}
        >
          Back
        </button>
      </div>
    );
  }
}

export default StudentDetails;
