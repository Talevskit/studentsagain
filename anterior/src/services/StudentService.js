import axios from 'axios';

const STUDENT_API = "http://localhost:8081/api/v1/";

class StudentService {

    getStudents(){
        return axios.get(STUDENT_API+"students")
    }
    createStudent(student){
        return axios.post(STUDENT_API+"student", student)
    }

    getStudentById(id){
        return axios.get(STUDENT_API+"student/"+id)
    }
    updateStudent(student, id){
        return axios.put(STUDENT_API+"student/"+id, student)
    }
    deleteStudent(id){
        return axios.delete(STUDENT_API+"/student/"+id)
    }

}
export default new StudentService();