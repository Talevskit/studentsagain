import FooterComponent from './components/FooterComponent';
import HeaderComponent from './components/HeaderComponent';
import ListStudentComponent from './components/ListStudentComponent';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import WelcomePageComponent from './components/WelcomePageComponent';
import CreateStudentComponent from './components/CreateStudentComponent';
import StudentDetails from './components/StudentDetails';

function App() {
  return (
    <div>
      <Router>
          <HeaderComponent />
            <div className="container">
              <Switch>
                  <Route exact path="/" component={WelcomePageComponent}></Route>
                  <Route path="/students" component={ListStudentComponent}></Route>
                  <Route path="/student/:id" component={CreateStudentComponent}></Route>
                  <Route path="/studentDetails/:id" component={StudentDetails}></Route>
              </Switch>
            </div>
          <FooterComponent />
      </Router>
    </div>
  );
}

export default App;
