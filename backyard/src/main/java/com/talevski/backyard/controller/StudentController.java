package com.talevski.backyard.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.talevski.backyard.exception.ResourceNotFoundException;
import com.talevski.backyard.model.Student;
import com.talevski.backyard.service.StudentService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class StudentController {
	@Autowired
	private StudentService studentService;
	
	@PostMapping("/student")
	public String saveStudent(@RequestBody Student student) {
		studentService.saveStudent(student);
		return "Student saved Succesfully";
	}
	@GetMapping("/students")
	public Iterable<Student> getStudents() {
		return studentService.getStudents();
	}
	@DeleteMapping("/student/{id}")
	public boolean deleteStudent(@PathVariable("id") int id) {
		studentService.deleteStudent(id);
		return true;
	}
	@GetMapping("/student/{id}")
	public Optional<Student> getStudent(@PathVariable("id") int id) {
		return Optional.ofNullable(studentService.getStudent(id).orElseThrow(()-> new ResourceNotFoundException("Student not found")));
		
	}
	@PutMapping("/student/{id}")
	public ResponseEntity<Student> updateStudent(@RequestBody Student student, @PathVariable("id") int id) {
		Student updated = studentService.getStudent(id).orElseThrow(() -> new ResourceNotFoundException("Student not found"));
		updated.setFirstName(student.getFirstName());
		updated.setLastName(student.getLastName());
		updated.setEmail(student.getEmail());
		updated.setAge(student.getAge());
		studentService.saveStudent(updated);
		return ResponseEntity.ok(updated);
	}

}
