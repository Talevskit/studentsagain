package com.talevski.backyard.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.talevski.backyard.model.Student;
import com.talevski.backyard.repository.StudentRepository;
@Service
public class StudentService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	public Student saveStudent(Student student) {
		studentRepository.save(student);
		return student;
	}
	
	public Iterable<Student> getStudents(){
		return studentRepository.findAll();
	}
		
	public Optional<Student> getStudent(int id) {
		return studentRepository.findById(id);
	}
	
	public void deleteStudent(int id) {
		studentRepository.deleteById(id);
	}
}
